iDeal Easy payment module for Ubercart

PREREQUISITES

- Drupal 6.X

INSTALLATION

Install and activate this module like every other Drupal
module. Ask ABN to redirect to http://www.yourdomain/ideal_easy_rtrn after payment

DESCRIPTION

Receive payments through checkout via Ideal Easy ABN/AMRO.

AUTHOR
C. Kodde
Qrios
www.qrios.nl
c.kodde (at) qrios {dot} nl

SPECIAL THANKS
Joris v. Tilburg

